<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/valet_icon.ico" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet"/>
    <link rel="stylesheet" href="css/layout.css"/>
    <title>Environment Setup with VALET: Command summary</title>
  </head>
  <body>
    <nav>
      Command Summary
      <span class="prev"><a href="index.html">introduction</a></span>
      <span class="next"><a href="envvars_and_files.html">env vars and files</a></span>
    </nav>
    <header class="valet"><img src="images/valet_icon_128.png"/><span>Environment Setup with VALET</span></header>
    <main>
      <section>
        <a name="getting_help"><h2>Getting help</h2></a>
        <p>Each of the VALET commands accepts the <code>-h</code> or <code>--help</code> flag to trigger the display of a built-in summary of itself.  In addition, a summary of the VALET commands that are available is provided by the <code>vpkg_help</code> command:</p>
        <pre class="code">[user@host ~]$ vpkg_help
        ⋮
  About the commands:

    vpkg_list

      List the available packages

    vpkg_versions &lt;pkgId&gt;

      List the versions available for a given package
        ⋮</pre>
      </section>
      <section>
        <a name="what_packages_are_available"><h2>What packages are available?</h2></a>
        <p>A list of available packages is displayed using the <code>vpkg_list</code> command:</p>
        <pre class="code">[user@host ~]$ vpkg_list

Available packages:
  in /home/1001/.valet
    blender
    fio
  in /opt/shared/valet/2.1/etc
    abaqus
    acml
    anaconda
    apache-ant
    atlas
      ⋮</pre>
        <p>For individual packages, a list of versions is displayed using the <code>vpkg_versions</code> command:</p>
        <pre class="code">[user@host ~]$ vpkg_versions gcc

Available versions in package (* = default version):

[/opt/shared/valet/2.1/etc/gcc.vpkg_xml]
gcc       GNU Compiler Suite
  4.8     alias to gcc/4.8.3
  4.8.3   Version 4.8.3
  4.9     alias to gcc/4.9.3
  4.9.3   Version 4.9.3
  6       alias to gcc/6.2
  6.2.0   Version 6.2.0
  6.2     alias to gcc/6.2.0
* system  OS-default GCC installation</pre>
        <p>The "*" on the last line indicates the version configured to be the default.</p>
        <p>A verbose description of a package or package version can be displayed using the <code>vpkg_info</code> command with a non-versioned package id (e.g. "gcc") or a versioned package id (e.g. "gcc/6"):</p>
        <pre class="code">[user@host ~]$ vpkg_info gcc/6.2.0
[gcc/6.2.0] {
  contexts: all
  Version 6.2.0
  prefix: /opt/shared/gcc/6.2.0
  standard paths: {
    bin: /opt/shared/gcc/6.2.0/bin
    lib: /opt/shared/gcc/6.2.0/lib, /opt/shared/gcc/6.2.0/lib64
    man: /opt/shared/gcc/6.2.0/share/man
    info: /opt/shared/gcc/6.2.0/share/info
    include: /opt/shared/gcc/6.2.0/include
    pkgConfig: /opt/shared/gcc/6.2.0/lib/pkgconfig
  }
}</pre>
          <div class="notes">While version 2.0 of VALET had the ability to locate and add standard paths (like <code>bin</code> and <code>lib</code>), the <code>vpkg_info</code> command did not provide an indication of which standard paths were actually found and would be added for the package version.  Version 2.1 addresses that confusing behavior by including that information in the output from <code>vpkg_info</code>.</div>
        <section>
          <a name="command_completion"><h3>Command completion</h3></a>
          <p>The bash shell can easily be extended to perform command completion on arbitrary commands.  VALET implements completion on the <code>vpkg_versions</code>, <code>vpkg_info</code>, <code>vpkg_require</code>, and <code>vpkg_devrequire</code> commands to aid the user in determining which package ids and version ids are available:</p>
          <pre class="code">[user@host ~]$ vpkg_info gcc<span style="color:yellow">&lt;tab&gt;&lt;tab&gt;</span>
gcc
[user@host ~]$ vpkg_info gcc/<span style="color:yellow">&lt;tab&gt;&lt;tab&gt;</span>
gcc/4.8     gcc/4.8.3   gcc/4.9     gcc/4.9.3   gcc/6       gcc/6.2     gcc/6.2.0   gcc/system
[user@host ~]$ vpkg_info gcc/4.<span style="color:yellow">&lt;tab&gt;&lt;tab&gt;</span>
gcc/4.8    gcc/4.8.3  gcc/4.9    gcc/4.9.3</pre>
        </section>
      </section>
      <section>
        <a name="applying_environment_changes"><h2>Applying environment changes</h2></a>
        <p>One or more packages are setup in the environment using the <code>vpkg_require</code> command:</p>
        <pre class="code">[user@host ~]$ vpkg_require gcc/6</pre>
        <p>Any problems encountered will result in no changes being made to the environment.  For example, trying to configure two versions of gcc simultaneously yields:</p>
        <pre class="code">[user@host ~]$ vpkg_require gcc/6 gcc/4.9
gcc/4.9.3 conflicts with gcc/6.2.0 already added to environment</pre>
        <p>Problems may also occur due to dependencies or incompatibilities that a version of a package has with respect to another package:</p>
        <pre class="code">[user@host ~]$ vpkg_require dummy gaussian
ERROR: incompatibility detected between a previously added package and gaussian/g03.c02
[user@host ~]$ vpkg_require gaussian dummy
ERROR: incompatibility detected between dummy/standard and gaussian/g03.c02</pre>
        <section>
          <a name="development_context"><h3>The development context</h3></a>
          <p>Often a package will contain code libraries or header files that other projects need to use.  The projects that consume these dependencies need to be informed of the directories in which to find these components.  For projects that use the GNU autotools, the <code>CPPFLAGS</code> and <code>LDFLAGS</code> environment variables can be set to include the appropriate flags and paths.  The <code>vpkg_devrequire</code> command requests that VALET make alterations to these two variables in addition to the rest of the changes it makes to the environment.</p>
          <p>Defining a package that lacks the <code>development</code> context means that it can never be used with the <code>vpkg_devrequire</code> command.  Declaring an environment variable action that applies only to the <code>development</code> context will only see it applied when the package is included under that context.  A context name can be communicated to VALET by the <code>--context=[context-name]</code> flag:</p>
          <pre class="code">[user@host ~]$ vpkg_require --context=development mct
Adding package `mct/2.8.3` to your environment
[user@host ~]$ echo $LDFLAGS
-L/opt/shared/mct/2.8.3/lib
[user@host ~]$ echo $CPPFLAGS
-I/opt/shared/mct/2.8.3/include</pre>
          <p>The command above is equivalent to using <code>vpkg_devrequire</code> on the <code>mct/2.8.3</code> package.</p>
        </section>
      </section>
      <section>
        <a name="undoing_environment_changes"><h2>Undoing environment changes</h2></a>
        <p>VALET makes a snapshot of your environment prior to each invocation of <code>vpkg_require</code>.  To undo the changes made by your last use of <code>vpkg_require</code> in your current shell, use the <code>vpkg_rollback</code> command:</p>
        <pre class="code">[user@host ~]$ vpkg_rollback
WARNING: no snapshots defined</pre>
        <p>The above message is displayed because <code>vpkg_require</code> had not yet been called in the current shell.  If <code>vpkg_require</code> has been called more than once it is possible to rollback through multiple levels of change by supplying the number of rollbacks to attempt. For example,</p>
        <pre class="code">[user@host ~]$ vpkg_rollback 2</pre>
        <p>would perform two rollbacks.  To remove all changes made to the environment in the current shell, the "all" argument can be used:</p>
        <pre>[user@host ~]$ vpkg_rollback all</pre>
      </section>
      <section>
        <a name="subshells"><h2>Subshells</h2></a>
        <p>Sometimes a user may spawn another shell as a child process of the current shell:</p>
        <pre class="code">[user@host ~]$ vpkg_require gcc/6 ; vpkg_devrequire mct
Adding package `gcc/6.2.0` to your environment
Adding package `mct/2.8.3` to your environment
[user@host ~]$ bash
[user@host ~]$ vpkg_rollback all
ERROR:  no environment snapshots for the current shell, unable to roll back</pre>
        <p>Though the subshell inherits most of the configuration applied by VALET (e.g. <code>PATH</code>, <code>LD_LIBRARY_PATH</code>) it does not inherit the environment snapshots which are specific to each running shell process.  However, the parent shell's snapshots can be propagated into the subshell if necessary:</p>
        <pre class="code">[user@host ~]$ vpkg_replicate_parent
DONE:  VALET rollback snapshots from 4a3f274a-41ec-3e9-384-3934de34 populated in this shell
[user@host ~]$ vpkg_rollback
[user@host ~]$ which gcc
/opt/shared/gcc/6.2.0/bin/gcc
[user@host ~]$ vpkg_rollback
[user@host ~]$ which gcc
/usr/bin/gcc</pre>
      </section>
      <section>
        <a name="history"><h2>History</h2></a>
        <p>To see what packages have been loaded into the environment of the current shell:</p>
        <pre class="code">[user@host ~]$ vpkg_history
[standard]
  gcc/6.2.0
[development]
  mct/2.8.3</pre>
        <p>Each successive invocation of <code>vpkg_require</code> (or <code>vpkg_devrequire</code>) is displayed as a section headed by the context that was chosen (in square brackets) and a list of versioned package ids.  Performing a rollback shows that the list runs from oldest invocation to newest:</p>
        <pre class="code">[user@host ~]$ vpkg_rollback
[user@host ~]$ vpkg_require matlab/r2016a mathematica/11
Adding package `matlab/r2016a` to your environment
Adding package `mathematica/11.0.0` to your environment
[user@host ~]$ vpkg_history
[standard]
  gcc/6.2.0
[standard]
  matlab/r2016a
  mathematica/11.0.0</pre>
      </section>
      <section>
        <a name="current_configuration"><h2>Current configuration</h2></a>
        <p><span class="version" title="avaiable in 2.1 or newer">2.1</span>There are many configurable options in VALET, some of which the system administrator alone can control and some that the user can control by means of environment variables.  The value of all configurational options can be summarized for the current shell:</p>
        <pre class="code">[user@host ~]$ valet_showConfig

VALET configuration summary:
  version                               : 2.1

  verbose                               : False
  shouldUseYAMLSafeLoadMethod           : True
  shouldAddFeatureTagsToVersionPrefixes : True
  shouldAddPackagePrefixAction          : True
  isExecStyleConfigDisabled             : False
  isPackageCountingEnabled              : False

  featureMatchingMode                   : least_additional

  prefixDir                             : /opt/shared/valet/2.1
  binDir                                : /opt/shared/valet/2.1/bin
  libExecDir                            : /opt/shared/valet/2.1/libexec
  sysConfDir                            : /home/1001/.valet
                                          /opt/shared/valet/2.1/etc
  blessedSysConfDir                     : /home/1001/.valet
                                          /opt/shared/valet/2.1/etc

  hwArchTags                            : x86_64
                                          intel
                                          intel/corei7-avx

  binPathDefaults                       : bin
                                          sbin
  libPathDefaults                       : lib
                                          lib64
                                          libso
  manPathDefaults                       : man
                                          share/man
  infoPathDefaults                      : share/info
  includePathDefaults                   : include
  pkgConfigPathDefaults                 : lib/pkgconfig
                                          share/pkgconfig</pre>
        <p>If the user sets <code>VALET_PATH</code> to point to a directory containing package definition files, the <code>sysConfDir</code> would naturally appear different:</p>
        <pre class="code">[user@host ~]$ export VALET_PATH=~/sw/valet
[user@host ~]$ export VALET_FEATURE_MATCH_MODE=most_additional
[user@host ~]$ valet_showConfig
     ⋮
  featureMatchingMode                   : most_additional
     ⋮
  sysConfDir                            : /home/1001/sw/valet
                                          /home/1001/.valet
                                          /opt/shared/valet/2.1/etc</pre>
        <p>The directories in <code>VALET_PATH</code> are searched prior to the system-defined directories, and in the order they are specified in the variable.</p>
      </section>
    </main>
  </body>
</html>
