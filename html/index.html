<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/valet_icon.ico" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet"/>
    <link rel="stylesheet" href="css/layout.css"/>
    <title>Environment Setup with VALET</title>
  </head>
  <body>
    <nav>
      Introduction
      <span class="next"><a href="commands.html">command summary</a></span>
    </nav>
    <header class="valet"><img src="images/valet_icon_128.png"/><span>Environment Setup with VALET</span></header>
    <main>
      <section>
        <a name="introduction"><h2>Introduction</h2></a>
        <p>One particularly annoying aspect of cluster computing for most users is getting the environment setup properly given a set of requisite libraries and applications. Quite often in a cluster environment there are multiple versions of a library available at any one time, and knowing which to use when running a program can often mean the difference between predictable, correct results and a flawed or even failed execution.</p>
        <p>A piece of software called (quite simply) "modules" has been around for quite some time to address the complexities of environment configuration for users. The tool itself is fairly complex, both in the commands it offers the user and the manner by which environment modifications are specified (a modulefile).</p>
        <p>VALET – a recursive acronym for VALET Automates Linux Environment Tasks – is an alternative that strives to be as simple as possible to configure and to use.</p>
      </section>
      <section>
        <a name="typical_changes_to_the_environment"><h2>Typical changes to the environment</h2></a>
        <p>There are often only a few kinds of changes that need to be made to the environment for a given software package, be it a library or an application:</p>
        <ul>
          <li>Directory(s) containing executables must be added to the <code>PATH</code></li>
          <li>Directory(s) containing shared libraries must be added to the <code>LD_LIBRARY_PATH</code></li>
          <li>Directory(s) containing man pages must be added to the <code>MANPATH</code></li>
          <li>The value of arbitrary environment variables must be set or augmented</li>
          <li>In a software development scenario, library and header directories must be added to the <code>LDFLAGS</code> and <code>CPPFLAGS</code> variables, respectively</li>
        </ul>
        <p>VALET handles these changes but also allows external scripts to be written that handle the more exotic (and less common) chores that may be required.</p>
        <section>
          <a name="encapsulating_changes"><h3>Encapsulating changes</h3></a>
          <p>The "modules" environment management software has the ability to unload changes it made:  remove any search paths that were added by a module, etc.  The capability is not very sophisticated and cannot account for complex changes made by external scripts that may have been called when the module was loaded.  VALET does not attempt to keep track of all changes that are made to the environment.  Instead, just before changes are made it creates a <em>snapshot</em> of the environment (functions, aliases, variables) and stores the data in a temporary file.  If the user does a <em>rollback</em>, the data in the temporary file are used to restore the environment represented therein.  This undoes <strong>any</strong> changes that were made by VALET, whether direct or due to some external script.</p>
          <p>For example, the Intel compiler suite includes several scripts that are meant to be sourced by the user to setup the compiler in his/her environment.  Intel also bundles specially-written module files (for the "modules" environment management software) that duplicate the changes those scripts affect.  VALET's environment snapshotting allows it to call the scripts Intel provides, rather than requiring that a set of VALET-specific files also duplicate what the scripts provide.</p>
          <p>The stateful nature of the snapshotting in VALET does introduce a complexity that "modules" does not possess.  Some amount of local storage is required (the directory referenced by the <code>TMPDIR</code> environment variable or  <code>/tmp</code> itself) and occasional cleanup is necessary to remove snapshots that are no longer tied to running shells.</p>
        </section>
      </section>
      <section>
        <a name="packages"><h2>Packages</h2></a>
        <p>In VALET, a <em>package</em> is a software component, where "component" can mean an application, a code library, a set of man pages, or a combination of all these pieces. For example, Mathematica would be an application: all the end user needs is for the appropriate directory to be on his/her <code>PATH</code> so that typing <code>mathematica</code> starts the program.  On the other hand, a program using Open MPI for parallelism might need both the <code>PATH</code> and <code>LD_LIBRARY_PATH</code> configured in order to function.</p>
        <p>As newer versions of software are released, the older versions are usually not removed immediately.  Users need time to adapt their workflow to changes in the software; occasionally the changes may be radical enough (e.g. Python 2 versus 3) that the workflow simply cannot be adapted to the new version.  A VALET package can have multiple <em>versions</em> associated with it.  Using the same examples as above, the system may have Mathematica 10 and 11 available to users.  Likewise, Open MPI 1.8.2 and 1.10.2 may be available, with multiple variants of each differing by the compiler suite used to build it.</p>
        <p>VALET uses a <em>versioned package id</em> similar to those used in "modules."  The versioned package id is a string containing at least two pieces, the package and the version, separated by a "/" (slash) character:</p>
        <pre class="code">openmpi/1.8.2
openmpi/1.8.8-intel64
openmpi/1.8.8-gcc-4.9
openmpi/1.10.2
mathematica/10
mathematica/10.0.2
mathematica/10.3.0
mathematica/11</pre>
        <p><span class="version" title="available in VALET 2.1 or newer">2.1</span>The version component of the identifier is internally broken into pieces that look like numeric version identifiers (e.g. "1.8.8" or "10") and simple strings (e.g. "-gcc-").  The numeric portions are compared and ordered logically:  "1.10.1" sorts after "1.8.2" rather than the other way under typical string ordering.</p>
        <p>A special form of versioned package id references whatever version of a package is indicated to be the default version:</p>
        <pre class="code">openmpi/default
mathematica/default</pre>
        <p>The "/" and version component can also be omitted to indicate the default version of a package:</p>
        <pre class="code">openmpi
mathematica</pre>
        <section>
          <a name="feature_tags"><h3>Feature tags</h3></a>
          <p><span class="version" title="available in VALET 2.0 or newer">2.0</span>In some cases a versioned package may have several variants, differing in the compiler used to build them, for example, or in the features enabled during the pre-build configuration of the source code.  In such cases, the version component can include the additional bits of identifying information:</p>
          <pre class="code">openmpi/1.8.2-intel2017-lustre</pre>
          <p>A better option is to use feature tags in the package identifier:</p>
          <pre class="code">openmpi/1.8.2:intel2017,lustre</pre>
          <p>Internally, feature tags form an ordered set of strings.  The package id above would be a match with any of the following requested package ids:</p>
          <pre class="code">openmpi/1.8.2:intel2017,lustre
  openmpi/1.8.2:intel2017
  openmpi/1.8.2:lustre
  openmpi/1.8.2:lustre,intel2017</pre>
          <p>When a package lookup is performed, any versioned package supporting at least those feature tags specified in the requested id is a match.</p>
        </section>
        <section>
          <a name="hardware_specific_sub_paths"><h3>Adaptive sub-paths</h3></a>
          <p><span class="version" title="available in VALET 2.1 or newer">2.1</span>VALET can also perform adaptive addition of sub-paths on paths to executables and libraries.  In previous versions, the specification of a <code>bin</code> directory yielded the addition of that directory to the user's <code>PATH</code> environment variable.  If the adaptive sub-paths feature is enabled, additional sub-paths (e.g. <code>x86_64</code>, <code>amd</code>, or <code>intel-avx2</code>) can also be detected and added to the <code>PATH</code>.  The sub-paths can be statically configured or can be generated programmatically by the <code>valet_hwarch</code> utility.  On a heterogeneous system this would allow VALET to alter the configured <code>PATH</code> according to the host capabilities.</p>
          <p>One organizational scheme that leverages this feature would be for each version of the software to have a common <code>bin</code> or <code>lib</code> directory containing generic versions of the software.  Hardware-optimized variants would be found in subdirectories named according to hardware tags:</p>
          <pre class="code">/opt/shared/exchange
  ├ 1.5.1/
  │ ├ bin/
  │ │ ├ calc_exchange
  │ │ ├ intel-avx2/
  │ │ │ ├ calc_exchange
  │ ├ lib/
  │ ├ man/</pre>
          <p>In this example, the <code>bin</code> directory contains the <code>calc_exchange</code> program compiled for generic x86 targets and <code>bin/intel-avx2</code> contains a copy optimized for AVX2 extensions.  If the <code>valet_hwarch</code> utility returns (among others) the tag "intel-avx2", then the <code>PATH</code> would be augmented with <code>/opt/shared/exchange/1.5.1/bin/intel-avx2:/opt/shared/exchange/1.5.1/bin</code> and the command <code>calc_exchange</code> would preferentially find the AVX2 variant.</p>
          <p>An alternative is to move the hardware-specificity directly under the package's root directory.  If the "exchange" package has a prefix of <code>/opt/shared/exchange</code> then hardware-optimized versions of the software would be installed in common subdirectories (e.g. <code>/opt/shared/exchange/intel-avx2</code>):</p>
          <pre class="code">/opt/shared/exchange
  ├ intel-avx2/
  │ ├ 1.5.1/
  │ │ ├ bin/
  │ │ │ ├ calc_exchange
  │ │ ├ lib/
  │ │ ├ man/
  ├ 1.5.1/
  │ ├ bin/
  │ │ ├ calc_exchange
  │ ├ lib/
  │ ├ man/
  ├ 1.6.0/
  │ ├ bin/
  │ │ ├ calc_exchange
  │ ├ lib/
  │ ├ man/</pre>
          <p>Provided <code>valet_hwarch</code> returns the "intel-avx2" tag, adding version "1.5.1" to the environment would use the prefix path <code>/opt/shared/exchange/intel-avx2/1.5.1</code>.  For version "1.6.0" the prefix path would be <code>/opt/shared/exchange/1.6.0</code> no matter what <code>valet_hwarch</code> returns.</p>
          <p>The important difference between these two schemes is that the first augments a baseline build while the second requires full builds of the entire software package.</p>
        </section>
        <section>
          <a name="contexts"><h3>Contexts</h3></a>
          <p><span class="version" title="available in VALET 2.1 or newer">2.1</span>The actions, dependencies, and incompatibilities associated with a package or a version of a package can be declared to be associated with one or more <em>contexts</em> (with the default <em>context</em> being <code>all</code>).  This allows a single package definition to hold unique behaviors that are selected by a command line flag.  Contexts can alleviate the need to create additional similar versions of a package (or an entire unique package definition) just to selectively add/subtract a few actions with respect to the environment.</p>
          <p>For example, consider a quantum chemistry program that includes multiple (mutually exclusive) basis definition libraries that are configured by setting the <code>BASIS_LIBRARY</code> environment variable.  Rather than creating a unique version definition for each possible basis library path, a single version defintion could contain multiple actions to set <code>BASIS_LIBRARY</code>, with each one having a unique context associated with it:</p>
          <pre class="code" type="yaml">   ⋮
actions:
  - variable: BASIS_LIBRARY
    value: "${VALET_PATH_PREFIX}/basis/default"
    contexts:
      - standard
  - variable: BASIS_LIBRARY
    value: "${VALET_PATH_PREFIX}/basis/high_precision"
    contexts:
      - high_precision
  - variable: BASIS_LIBRARY
    value: "${VALET_PATH_PREFIX}/basis/transition_metals"
    contexts:
      - transition_metals
   ⋮ </pre>
          <p>Thus, based on the context indicated by the user, a different path is added to <code>BASIS_LIBRARY</code>.</p>
          <p>VALET users are welcome to use any context names in their own package definitions with the exception of the reserved names:</p>
          <ul>
            <li><code>all</code></li>
            <li><code>standard</code></li>
            <li><code>development</code></li>
          </ul>
          <p>It may be wise for users to qualify their own context names with a common prefix; an approach similar to XML namespaces might be appropriate.  For example, user "marty" might qualify his personally-defined context name for GUI-based components as <code>marty:interactive</code>.  The name of the package in which the contexts are pertinent (as in the example above) would work well, too:  <code>fake_qc:transition_metals</code>.</p>
          <div class="notes">Each version of a package (and the package itself) can also have one or more contexts with which it is associated.  By default they receive the <code>all</code> context and in most cases that is probably appropriate.  Using other contexts implies that those versions or packages are not "visible" under other contexts.</div>
      </section>
    </main>
  </body>
</html>
