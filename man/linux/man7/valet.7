.de URL
\\$2 \(laURL: \\$1 \(ra\\$3
..
.if \n[.g] .mso www.tmac
.TH VALET 7 2016-11-02 "" "VALET User Manual"
.SH NAME
valet \- VALET Automates Linux Environment Tasks
.SH DESCRIPTION
One particularly annoying aspect of cluster computing for most users is getting the environment setup properly given a set of requisite libraries and applications. Quite often in a cluster environment there are multiple versions of a library available at any one time, and knowing which to use when running a program can often mean the difference between predictable, correct results and a flawed or even failed execution.
A piece of software called (quite simply) "modules" has been around for quite some time to address the complexities of environment configuration for users. The tool itself is fairly complex, both in the commands it offers the user and the manner by which environment modifications are specified (a modulefile).
.B valet
-- a recursive acronym for VALET Automates Linux Environment Tasks -- is an alternative that strives to be as simple as possible to configure and to use, while adding features not present in the "modules" software.
.PP
There are often only a few kinds of changes that need to be made to the environment for a given software package, be it a library or an application:
.IP \(bu 4
Directory(s) containing executables must be added to the PATH
.IP \(bu 4
Directory(s) containing shared libraries must be added to the LD_LIBRARY_PATH
.IP \(bu 4
Directory(s) containing man pages must be added to the MANPATH
.IP \(bu 4
The value of arbitrary environment variables must be set or augmented
.IP \(bu 4
In a software development scenario, library and header directories must be added to the LDFLAGS and CPPFLAGS variables, respectively
.PP
.B valet
handles these changes but also allows external scripts to be written that handle the more exotic (and less common) chores that may be required.
.SH PACKAGE IDENTIFICATION
In
.B valet
a package is a software component, where "component" can mean an application, a code library, a set of man pages, or a combination of all these pieces. For example, Mathematica would be an application: all the end user needs is for the appropriate directory to be on his/her PATH so that typing mathematica starts the program). On the other hand, a program using Open MPI for parallelism might need both the PATH and LD_LIBRARY_PATH configured in order to function.
.PP
As newer versions of software are released, the older versions are usually not removed immediately when the new are installed on a system. Users need time to adapt their workflow to changes in the software; occasionally the changes may be radical enough (e.g. Python 2 versus 3) that the workflow simply cannot be adapted to the new version. A
.B valet
package can have multiple versions associated with it. Using the same examples as above, the system may have Mathematica 6, 7 and 8 available to users. Likewise, OpenMPI 1.3.3 and 1.4.2 may be available.
.PP
.B valet
uses a versioned package id similar to those used in "modules". The versioned package id is a string containing at least two pieces, the package and the version, separated by a "/" character:
.PP
.IP \(bu 4
mathematica/6
.IP \(bu 4
mathematica/7
.IP \(bu 4
mathematica/8
.IP \(bu 4
openmpi/1.3.3
.IP \(bu 4
openmpi/1.3.3-mx
.IP \(bu 4
openmpi/1.4.2
.IP \(bu 4
openmpi/1.4.2-mx
.PP
The version component of the identifier is internally broken into components that look like numeric version identifiers (e.g. "1.4.2" or "8") and simple strings (e.g. "-mx").  The numeric portions are compared and ordered logically:  "1.10.1" sorts after "1.8.2" rather than the other way under typical string ordering.
.PP
A special form of versioned package id references whatever version of a package is indicated to be the default version:
.PP
.IP \(bu 4
mathematica/default
.IP \(bu 4
openmpi/default
.PP
The "/" and version information can also be omitted to indicate the default version of a package:
.PP
.IP \(bu 4
mathematica     is equivalent to     mathematica/default
.PP
In some cases a versioned package may have several variants, differing in the compiler used to build them, for example, or in the features enabled during the pre-build configuration of the source code.  In such cases, the version component can include the additional bits of identifying information:
.PP
.IP \(bu 4
openmpi/1.8.2-intel2017-lustre
.PP
But a better option is to use
.I feature tags
in the package identifier:
.PP
.IP \(bu 4
openmpi/1.8.2:intel2017,lustre
.PP
Internally, feature tags form an ordered set of strings.  The package id above would be a match with any of the following requested package ids:
.PP
.IP \(bu 4
openmpi/1.8.2:intel2017,lustre
.IP \(bu 4
openmpi/1.8.2:intel2017
.IP \(bu 4
openmpi/1.8.2:lustre
.IP \(bu 4
openmpi/1.8.2:lustre,intel2017
.PP
When a package lookup is performed, any versioned package supporting at least those feature tags specified in the requested id is a match.
.SS ADAPTIVE SUB-PATHS
A new feature in
.B valet
2.1 is adaptive addition of sub-paths on paths to executables and libraries.  In previous versions, the specification of a "bin" directory yielded the addition of that directory to the user's PATH environment variable.  In
.B valet
2.1, additional sub-paths (e.g. x86_64, amd, or intel-avx2) can also be detected and added to the PATH.  The sub-paths can be statically configured or can be generated programmatically by the
.I valet_hwarch
utility.  On a heterogeneous system this would allow
.B valet
to alter the configured PATH according to the host capabilities.
.PP
One organizational scheme that leverages this feature would be for each version of the software to have a common "bin" or "lib" directory containing generic versions of the software.  Hardware-optimized variants would be found in subdirectories named according to the hardware tags.  For example, the "bin" directory might contain the "calc_exchange" program compiled for generic x86 targets and "bin/intel-avx2/calc_exchange" would contain a copy optimized for AVX2 extensions.  If the
.I valet_hwarch
utility returns (among others) the tag "intel-avx2", then the PATH would be augmented with "<prefix>/bin/intel-avx2:<prefix>/bin" and the command "calc_exchange" would preferentially find the AVX2 variant.
.PP
An alternative is to move the hardware-specificity directly under the package's root directory.  If the "exchange" package has a prefix of
.I /opt/shared/exchange
then hardware-optimized versions of the software would be installed in common subdirectories (e.g.
.I /opt/shared/exchange/intel-avx2
) and provided
.I valet_hwarch
returns the "intel-avx2" tag, any <version> of the "exchange" package for which
.I /opt/shared/exchange/intel-avx2/<version>
exists would use that as its prefix path.  Otherwise, the path
.I /opt/shared/exchange/<version>
is the prefix path.
.PP
The important difference between these two schemes is that the first augments a baseline build while the second requires full builds of the entire software package.

.SH COMMANDS
.B valet
shell commands are prefixed with "vpkg_" and a quick summary of them can be displayed with the
.BR vpkg_help (1),
command.  A manual page also exists for each individual command.
.SH ENVIRONMENT
.TP 4
VALET_PREFIX
Path to the
.B valet
installation on the machine.  The system-wide default .vpkg files can be found in the "etc" subdirectory of VALET_PREFIX.
.TP 4
VALET_PATH
Colon-delimited list of paths that should be sequentially searched for package definition files.  Allows users to create their own package definition files that augment or override the package definition files defined by the system administrator.  The ~/.valet directory (if it exists) and system default directory are implicitly included at the end of the list (in that order).
.TP 4
VALET_VERBOSE
Can be used to override the default verbosity level set by the system administrator.  Valid values are YES, TRUE, NO, FALSE, integer zero (false), and non-zero integer (true).
.TP 4
VALET_FEATURE_MATCH_MODE
Can be used to override the default feature-matching mode set by the system administrator.  When multiple versions of a package match with a requested versioned package id, a single match is selected according to this criteria.  Valid values are FIRST_MATCH (arbitrary), LEAST_ADDITIONAL (lowest count of unrequested features), or MOST_ADDITIONAL (highest count of unrequested features).
.TP 4
VALET_BLESSED_PATH
Colon-delimited list of additional package definition directories in which the user would like to allow executable file formats to be processed (see
.BR vpkg_py (5)
for more information).  The system administrator can disable all executable formats using the
.I isExecStyleConfigDisabled
field in
.I valet.config
.SH FILES
.TP 4
${VALET_PREFIX}/lib/python/valet/config.py
The default configuration file for an installation of
.B valet
 .  This file should be edited by the system administrator.
.TP 4
${VALET_PREFIX}/etc
The default location for package definition files.
.TP 4
~/.valet
Per-user directory containing package definition files.
.TP 4
${VALET_PREFIX}/libexec
The default location for supporting executables that may be called by package definitions.  When an executable action uses a relative path, it is taken to be relative to this directory.
.TP 4
/etc/sysconfig/valet
System-wide configuration of
.B valet
environment variables.
.TP 4
~/.valetrc
Per-user configuration of
.B valet
environment variables.  Overrides values set in
.B /etc/sysconfig/valet
.SH SEE ALSO
.BR vpkg_help (1),
.BR vpkg_list (1),
.BR vpkg_versions (1),
.BR vpkg_info (1),
.BR vpkg_attr (1),
.BR vpkg_require (1),
.BR vpkg_devrequire (1),
.BR vpkg_rollback (1)
.SH HISTORY
.TP 8
1.0
October 2011 (original release)
.TP 8
2.0
January 2014 (feature tags, JSON support)
.TP 8
3.0
July 2018 (restructured parsing engine, YAML/Python support added, contexts, flags, CSH support)
.SH AUTHORS
The
.B valet
software is written, documented, and supported by Dr. Jeffrey Frey, Network & Systems Services at the University of Delaware.  Please direct all comments, suggestions, and bugs to him via email at frey@udel.edu.  The
.B valet
website can be found at:
.PP
.URL "https://gitlab.com/valet" "valet project" "at gitlab"
.\" $Id$
