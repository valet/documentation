.TH VALET 5 2016-11-07 "" "VALET Package Definition Files"
.SH NAME
valet \- JSON package definition file format
.SH DESCRIPTION
.B valet
2.0 introduced a JSON variant of the package definition file.  The format uses the
.B .vpkg_json
extension.  JSON — short for JavaScript Object Notation — is an open standard format that uses human-readable text to represent structured data, much like XML does.  XML carries with it a heavy amount of additional formatting surrounding the data, which some people find makes the data itself less visible when inspected.  JSON uses far less formatting around the data.  Container syntaxes (arrays and dictionaries) are built into JSON, whereas in XML it is up to the document schema or programmer to define and detect such representations.
.PP
The JSON standard does not include a syntax for embedding comments within a document.  Since comments are very helpful in configuration documents,
.B valet
augments the standard JSON syntax with hash-delimited comment lines, in the style of BASH and Python.  Any line prefixed with optional whitespace followed by a pound sign (hash, "#") is discarded.  Only full lines are matched:  the parser does not recognize a comment at the end of a line containing JSON content.  For example:
.PP
.RS 4
.nf

 # A comment
   # can have leading whitespace
   # before the hash sign
   "prefix":  "/opt/shared/valet",  # but this is not valid

.fi
.RE
.PP
would produce a JSON parsing error since the comment on the "prefix" key-value pair would not be stripped from the text.
.PP
Care must also be taken in JSON documents to never leave a trailing comma on the last element in a collection.  An array like
.PP
.RS 4
.nf

  "dependencies": [ "openmpi/1.10.2+intel2016", "fftw/3.2+intel2016", ]

.fi
.RE
.PP
would produce a parsing error.  The YAML format is very similar to JSON but includes syntax for comments and allows trailing commas in collections defined using the brace syntax.  If JSON seems too rigid, users are encouraged to try the YAML package definition syntax in
.B valet
3.0.
.SS PACKAGE
A
.B valet
package is represented in JSON as a dictionary containing a single key-value pair:
.PP
.RS 4
.nf
{ "<<pkg-id>>": {
      ... definition ...
  }
}
.fi
.RE
.PP
The "<<pkg-id>>" must match with the name of the file itself.  For example, mathematica.vpkg_json should contain JSON resembling:
.PP
.RS 4
.nf
{ "mathematica": {
    "description":   "Wolfram Mathematica",
    "url":           "http://www.wolfram.com/",
    "prefix":        "/opt/shared/mathematica"
  }
}
.fi
.RE
.PP
The set of permissible keys that can be used both in the package dictionary as well as any version dictionaries is:
.PP
.TP 4
.I description
String value; human-readable description of the software package
.TP 4
.I url
String value; a web site documenting the package, for example
.TP 4
.I prefix
String value; filesystem path containing the package or the version of the package
.TP 4
.I development-env
Boolean value; value indicates whether or not
.B valet
should emit LDFLAGS and CPPFLAGS (included for 2.0 compatibility, see the
.I flags
key)
.TP 4
.I no-development-env
Boolean value; value indicates whether or not
.B valet
should not emit LDFLAGS and CPPFLAGS (included for 2.0 compatibility, see the
.I flags
key)
.TP 4
.I standard-paths
Boolean value; value indicates whether or not
.B valet
should check for standard sub-paths like "bin" and "lib" under the prefix directory (included for 2.0 compatibility, see the
.I flags
key)
.TP 4
.I no-standard-paths
Boolean value; value indicates whether or not
.B valet
should not check for standard sub-paths like "bin" and "lib" under the prefix directory (included for 2.0 compatibility, see the
.I flags
key)
.TP 4
.I contexts
Array of string values; contexts under which this package (or version of a package) is available
.TP 4
.I flags
Array of string values; currently defined flags include
.PP
.RS 4
.IP \(bu 4
.I no-standard-paths
: inhibit the automatic inclusion of standard paths in environment setup of this package/versioned package
.IP \(bu 4
.I no-development-env
: inhibit the addition of standard paths to the CFLAGS and LDFLAGS environment variables
.RE
.PP
The package dictionary can also have the default version (to be used when the user provides a bare package id to commands like
.B vpkg_require
) specified using the
.I default-value
key.  This key is not used inside a version dictionary.
.PP
Typically, the package dictionary
.I prefix
is set to a directory containing each version of the package, and each versioned package defines a prefix relative to that directory:
.PP
.RS 4
.nf
$ ls -l /opt/shared/mathematica
total 14
drwxr-sr-x 8 user   sysadmin 8 Aug 27 16:10 6.0.1
drwxr-sr-x 8 user   sysadmin 8 Aug 27 16:10 10.2.0
drwxr-sr-x 8 user   sysadmin 8 Aug 28 09:26 10.4.0
drwxrwsr-x 2 user   sysadmin 3 Aug 27 17:10 attic
.fi
.RE
.PP
The package's prefix would be "/opt/shared/mathematica" (absolute path required) and versions would use e.g. a prefix or "6.0.1" (relative or absolute path).  If no prefix is provided for a versioned package, then
.B valet
implicitly uses the version id.
.SS VERSION
Versions of a package are represented as key-value pairs in a dictionary keyed by the string "versions" inside the package dictionary:
.PP
.RS 4
.nf
{
  "mathematica": {
    "description":         "Wolfram Mathematica",
    "url":                 "http://www.wolfram.com/",
    "prefix":              "/opt/shared/mathematica",
    "flags":               ["no-development-env", "no-standard-paths"],
    "default-version":     "10",
    "versions": {
      "6": {
        "description":     "Wolfram Mathematica version 6",
        "url":             "http://www.wolfram.com/6",
        "prefix":          "6.0.1"
      },
      "10.2.0": {
        "description":     "Wolfram Mathematica version 10.2"
      },
      "10.4.0": {
        "description":     "Wolfram Mathematica version 10.4"
      },
      "10.4": { "alias-to": "10.4.0" },
      "10": { "alias-to": "10.4" }
    }
  }
}
.fi
.RE
.PP
The optional
.I description
and
.I url
key-value pairs override the value presented in the parent package element.
.PP
As mentioned earlier, the prefix value specified here may be a path relative to the prefix provided by the parent package.  In this case "/opt/mathematica/6.0.1" would be the full path to the versioned package with version id "6.0.1."  For versioned package dictionaries that lack an explicit prefix, the version id will be used in its stead as a path component relative to the parent package's prefix.  In this example the "10.4.0" versioned package would have the implicit prefix "/opt/mathematica/10.4.0" since it had no explicit prefix.
.PP
Version aliases can be used to provide alternate names for a package.  In this case, the version id "10.4" and "10" are both aliases that refer to the "10.4.0" release of Mathematica.
.PP
For the available
.I flags
, a versioned package includes any flag strings declared on itself in addition to any flag strings declared for the parent package.  In the example above, all versions of Mathematica inherit the "no-development-env" and "no-standard-paths" flags that were specified on the parent package.
.PP
Once the overall versioning structure of a package definition has been created, the environment tests and changes associated with the package and versions of the package can be added. The three kinds of tests/changes are:
.TP 4
.I dependencies
other packages that must be present or loaded into the environment; environment variable tests that must evaluate to true for configuration to happen
.TP 4
.I incompatibilities
other packages that must NOT be present in the environment; environment variable tests that must evaluate to false for configuration to happen
.TP 4
.I actions
changes to be made to the environment if all dependencies and incompatibilities are satisfied
.PP
If present in the package dictionary, the tests/actions affect all versions of the package and are satisfied/applied before those present inside a version dictionary.  For example:
.PP
.RS 4
.nf
{
  "mathematica": {
    "description":         "Wolfram Mathematica",
    "url":                 "http://www.wolfram.com/",
    "prefix":              "/opt/shared/mathematica",
    "flags":               ["no-development-env", "no-standard-paths"],
    "default-version":     "10",
    "actions":             [ { "method": "exec", "order": "success-first", "succeed": 0,
                               "target": { "any": "mk-user-scratchdir" } },
                             { "bindir":   "Executables" } ],
    "versions": {
      "6": {
        "description":     "Wolfram Mathematica version 6",
        "url":             "http://www.wolfram.com/6",
        "prefix":          "6.0.1",
        "actions":         [ { "contexts": ["development"],
                               "variable": "MATHEMATICA_MAJOR", "value": "6" } ]
      },
      "10.2.0": {
        "description":     "Wolfram Mathematica version 10.2",
        "actions":         [ { "contexts": ["development"],
                               "variable": "MATHEMATICA_MAJOR", "value": "10" } ]
      },
      "10.4.0": {
        "description":     "Wolfram Mathematica version 10.4",
        "actions":         [ { "contexts": ["development"],
                               "variable": "MATHEMATICA_MAJOR", "value": "10" } ]
      },
      "10.4": { "alias-to": "10.4.0" },
      "10": { "alias-to": "10.4" }
    }
  }
}
.fi
.RE
.PP
Given the package definition above, the command
.B vpkg_require mathematica/10.4
would yield the following series of events:
.IP 1. 4
The version "10.4" is an alias to "10.4.0," so "10.4.0" becomes the target
.IP 2. 4
The prefix is constructed as "/opt/shared/mathematica/10.4.0"
.IP 3. 4
The file at the path "${VALET_PREFIX}/libexec/mk-user-scratchdir" is executed.
.RS 4
.IP \(bu 4
If the return code is non-zero, the configuration fails and
.B vpkg_require
exits
.RE
.IP 4. 4
The path "/opt/shared/mathematica/10.4.0/Executables" (if it exists) is prepended to the PATH environment variable.
.IP 5. 4
If the "development" context has been chosen, the environment variable MATHEMATICA_MAJOR is set to have the value "10."
.PP
If control reaches the end of this pseudo-code, then the mathematica/10.4.0 package has been successfully configured in the environment.
.SS ACTIONS
An
.I actions
array can be specified either in a package or a version dictionary and contains zero or more elements of the following kinds:
.TP 4
variable
make changes to environment variables
.TP 4
special directories
shorthand for alterations to PATH, LD_LIBRARY_PATH, etc.
.TP 4
executable
source a script or execute a program
.TP 4
shell alias
set/unset an alias
.PP
Each action is represented as a dictionary.  Actions attached to the package itself will be performed when any version of the package is selected for addition to the environment, and in addition to actions associated with the version itself.
.PP
Actions are performed in the order they are specified.  Example:
.PP
.RS 4
.nf
"actions": [
      { "incdir": "/opt/include" },
      { "variable": "PATH", "operator": "scrub-path", "value": "/opt/bin" },
      { "bindir": "/opt/bin",
        "libdir": ["/opt/lib", "/opt/lib64"],
        "mandir": "/opt/share/man"
      },
      { "variable": "USER_SW_PATH", "value": "/opt/bin" },
      { "variable": "CFLAGS", "operator": "append-space", contexts=["development"],
        "value": "-O3 -g"
      }
  ]
.fi
.RE
.PP
.RS 4
.SS Variable actions
Variable actions alter the value of an environment variable.  A variable action is a dictionary with the following key-value pairs:
.TP 4
.I variable
Environment variable name	(required)
.TP 4
.I operator
One of the following:
.RS 4
.IP \(bu 4
set (default)
.IP \(bu 4
unset
.IP \(bu 4
append
.IP \(bu 4
prepend
.IP \(bu 4
append-path
.IP \(bu 4
prepend-path
.IP \(bu 4
append-space
.IP \(bu 4
prepend-space
.IP \(bu 4
scrub
.IP \(bu 4
scrub-path
.RE
.TP 4
.I contexts
A list of the contexts under which this action should be performed (defaults to "all")
.TP 4
.I value
The value to add/remove/etc.
.PP
The operators are self-explanatory for the most part.  The
.I scrub
operator removes all occurrences of
.I value
from the variable's current value in the environment.  The
.I scrub-path
operator treats the value of variable as a search path and removes from that search path all occurrences (by exact match) of
.I value
\[char46]
.PP
The
.I value
may contain references to other environment variables using the syntax ${ENV VAR NAME}.
.PP
.RS 4
.nf
{ "variable": "PATH", "operator": "scrub-path", "contexts": ["development"],
  "value": "${HOME}/bin" }
.fi
.RE
.SS Special directories
Special-directory actions are shortcuts that produce variable actions that affect standard environment variables (like PATH or LD_LIBRARY_PATH).  A special-directory action is a dictionary containing one or more key-value pairs, where the values are strings or arrays of strings, and the keys are:
.TP 4
.I bindir
Prepend path(s) to PATH
.TP 4
.I libdir
Prepend path(s) to LD_LIBRARY_PATH (and append to LDFLAGS under the "development" context)
.TP 4
.I incdir
Append path(s) to CPPFLAGS under the "development" context
.TP 4
.I mandir
Prepend path(s) to MANPATH
.TP 4
.I infodir
Prepend path(s) to INFOPATH
.TP 4
.I pkgconfigdir
Prepend path(s) to PKG_CONFIG_PATH
.PP
The values (string or array of strings) should be absolute paths or paths relative to the versioned package's prefix.
.PP
.RS 4
.nf
{ "bindir": "/opt/bin",
  "libdir": [ "/opt/lib", "/opt/lib64" ],
  "mandir": ["share/man"]
}
.fi
.RE
.PP
Note the
.I mandir
value is an array of just a single string, so it could also have been expressed as just a string, rather than an array; since the path "share/man" is relative, it is relative to the chosen version's prefix.
.PP
.SS Executables
At times
.B valet
may not be able to fully express the modifications necessary for a package, or the package includes its own extensive environment configuration scripts.  In such instances, a
.B valet
package can be configured to source an external script or execute a program.  An executable action is a dictionary with the following keys:
.TP 4
.I method
Either "source" (default) or "exec"
.TP 4
.I order
Either "failure-first" or "success-first"
.TP 4
.I success
integer return code indicating successful execution/sourcing
.TP 4
.I failure
integer return code indicating failed execution/sourcing
.TP 4
.I target
dictionary of paths keyed by shell types
.TP 4
.I contexts
A list of the contexts under which this action should be performed (defaults to "all")
.PP
The
.I order
determines which is tested first, the success return code or the failure return code.  If no success or failure are specified, then the return code is not tested.  Many shell scripts return code 0 on success and a non-zero value in case of a failure; this could be tested by supplying
.I order
"success_first" and
.I success
0.
.PP
.RS 4
.nf
{ "method": "source", "order": "success-first", "success": 0,
  "target": {
    "sh": "/opt/mathematica/bin/remote_kernel.sh",
    "csh": "/opt/mathematica/bin/remote_kernel.csh"
  }
}
.fi
.RE
.SS Shell aliases
An alias is a word that represents an arbitrarily complex command in the shell environment.  Rather than typing the command "ls -l | less" repeatedly, a user might create an alias named "lll" that when typed yields the same results as if "ls -l | less" had been typed.  A shell-alias action is a dictionary with the following keys:
.TP 4
.I shell-alias
the word that should be aliased to the arbitrarily complex command
.TP 4
.I command
dictionary of command strings keyed by shell types
.TP 4
.I contexts
A list of the contexts under which this action should be performed (defaults to "all")
.PP
Often times a shell alias supplies a fixed set of arguments to a command that is not shell-specific.  In such cases, using the shell type "any" or "*" is desirable:
.PP
.RS 4
.nf
{ "shell-alias": "lsadl",
  "command": {
    "*": "ls -ald",
  }
}
.fi
.RE
.SS DEPENDENCIES & INCOMPATIBILITIES
Dependencies and incompatibilities are arrays that contain one or more package identifiers, package identifier patterns, or predicates that must:
.IP \(bu 4
dependencies:
.RS 4
.IP \(bu 4
package id/pattern must be satisfied (present in environment or successfully loads and configures)
.IP \(bu 4
predicate evaluates to true
.RE
.IP \(bu 4
incompatibilities:
.RS 4
.IP \(bu 4
package id/pattern must not be configured in the current environment
.IP \(bu 4
predicate evaluates to false
.RE
.PP
A predicate should have a
.I stage
key-value pair that indicates when it should be tested.  A
.I pre-condition
is tested before any environment changes are made for the sake of the package, while a
.I post-condition
is tested after all environment changes have been made for the sake of the package.
.PP
.RS 4
.nf
"dependencies": [ "pgi/14", "fftw/^^3\." ],
"incompatibilities": [
  { "stage": "post-condition",
    "path": "${GAUSS_SCRDIR}",
    "operator": "path-under",
    "value": "/home",
    "message": "Home directories should not be used as scratch for Gaussian."
  }
]
.fi
.RE
.RS 4
.SS Package id patterns
A package id pattern is a string that is similar in construction to a regular package id but contains
.IP \(bu 4
a regular expression for the package id, version id, or both
.IP \(bu 4
a version predicate for the version id
.PP
A regular expression is indicated in either half by prefixing with a carat (^) character. For example, the pattern
.PP
.RS 4
openmpi/^^1\\.[0-9]*[02468]\\.
.RE
.PP
includes a regular expression in the version half of the id.  Discarding the leading carat, the regular expression is
.PP
.RS 4
^1\\.[0-9]*[02468]\\.
.RE
.PP
This pattern will match with any even-numbered release of the openmpi package in the 1.x series: 1.4.4, 1.6.3, 1.8.2 would all be matches, but 1.5.1 would not.
.PP
As a dependency, id patterns are tested against packages that have already been loaded into the environment.  If no matching package has been loaded,
.B valet
locates an available package that matches the pattern and attempts to load it into the environment.  Note that
.B valet
chooses the first versioned package it finds as a match, which may or may not be a new enough version, etc.  Ideally, the user will have loaded an appropriate version of the package prior to loading the package that uses an id pattern dependency.
.PP
.RS 4
.B [NOTE]:
Using a regular expression or version predicate that is the logical negation of that which you would use for a dependency (thus employing an incompatibility) forces the user to manually load the dependency prior to loading this package.  This is because package ids and id patterns inside an
.I <incompatibilities>
element do not cause
.B valet
to attempt to load a package into the environment.
.PP
A version predicate is prefixed with an equals (=) character and outlines numerical version tests to be made against the defined versions of a package.  For example, selecting any release of Open MPI after and including 1.4.4 but prior to 1.10 uses the pattern:
.PP
.RS 4
openmpi/=>=1.4.4&<1.10
.RE
.PP
The syntax is simple:  discarding the leading equals (=) character, one or more tests are linked using logical AND (&) and logical OR (|).  Each test consists of a binary operator and a version number:
.PP
.RS 4
  (=|==|<|<=|>=|>)(X(.Y(.Z)?)?)
.RE
.PP
The left-hand side of the test is the primary version number (the first found moving from left to right) that
.B valet
finds in the version id.  So the version id "1.10.2+intel2016+lustre" would have a primary version number "1.10.2" while the version id "release-candidate-2016" would have a primary version number "2016."  If a version id has no numeric versioning components present, then no version predicate will match it (for example, version id "os-native").  When specifying the right-hand side, only the first numerical component is required.  Omitting the dotted second or third components, the predicate will only be against the provided components.  Thus, a L.H.S. version id "1.10.2" matches all of the following predicates:
.PP
.IP \(bu 4
=1.10.2
.IP \(bu 4
=1.10
.IP \(bu 4
=1
.IP \(bu 4
>=1.10
.IP \(bu 4
<2
.PP
For "=1.10" and "=1" the missing second and third version numbers are NOT assumed to be zero, but are simply not included in the comparison.  Since "1.10.2" has it's first numerical component "1" equal to "1," the predicate "=1" is satisfied.  Likewise, since the second numerical component "10" equals "10," the predicate "=1.10" is also satisfied.
.SS Predicates
A predicate is a test performed against an environment variable's value to determine whether package configuration should succeed or fail.  Minimally, a predicate dictionary must contain an environment variable name and an operator that specifies what test is to be performed.
.PP
A predicate dictionary should have a
.I stage
key that has one of the following values:
.IP \(bu 4
.I pre-condition
(default):  test must be satisfied BEFORE configuration changes are made
.IP \(bu 4
.I post-condition
:  test must be satisfied AFTER configuration changes are made
.PP
Additionally, the dictionary should also have the following keys:
.TP 4
.I variable
environment variable name; for filesystem predicates, the path to be tested
.TP 4
.I operator
One of the following:
.RS 4
.IP \(bu 4
.I is-set
: has a value
.IP \(bu 4
.I is-not-set: does not have a value
.IP \(bu 4
.I eq ==
: lexigraphically equivalent to
.I value
.IP \(bu 4
.I ne !=
: not lexigraphically equivalent to
.I value
.IP \(bu 4
.I lt <
: lexigraphically less-than
.I value
.IP \(bu 4
.I le <=
: lexigraphically less-than or equivalent to
.I value
.IP \(bu 4
.I gt >
: lexigraphically greater-than
.I value
.IP \(bu 4
.I ge >=
: lexigraphically greater-than or equivalent to
.I value
.IP \(bu 4
.I starts-with <<
:
.I value
as prefix
.IP \(bu 4
.I not-starts-with !<<
: not
.I value
as prefix
.IP \(bu 4
.I ends-with >>
:
.I value
as suffix
.IP \(bu 4
.I not-ends-with !>>
: not
.I value
as suffix
.IP \(bu 4
.I contains <>
: contains
.I value
.IP \(bu 4
.I not-contains !<>
: does not contain
.I value
.IP \(bu 4
.I matches ~
: matches regular expression in
.I value
.IP \(bu 4
.I not-matches !~
: does not match regular expression in
.I value
.IP \(bu 4
.I exists -e
: path specified exists on system
.IP \(bu 4
.I not-exists !-e
: path specified does not exist on system
.IP \(bu 4
.I is-file-type -t
: path specified is of
.I value
file type
.IP \(bu 4
.I not-is-file-type !-t
: path specified is not of
.I value
file type
.IP \(bu 4
.I is-strict-file-type -st
: path specified is of
.I value
file type (symlinks not followed)
.IP \(bu 4
.I not-is-strict-file-type !-st
: path specified is not of
.I value
file type (symlinks not followed)
.IP \(bu 4
.I is-readable -r
: path specified is readable by current user
.IP \(bu 4
.I not-is-readable !-r
: path specified is not readable by current user
.IP \(bu 4
.I is-writable -w
: path specified is writable by current user
.IP \(bu 4
.I not-is-writable !-w
: path specified is not writable by current user
.IP \(bu 4
.I is-executable -x
: path specified is executable by current user
.IP \(bu 4
.I not-is-executable !-x
: path specified is not executable by current user
.IP \(bu 4
.I path-under is-descendent-of />
: path specified is a descendent path of
.I value
; the path is canonicalized before testing to eliminate symbolic links, etc.
.IP \(bu 4
.I not-path-under not-is-descendent-of !/>
: path specified is not a descendent path of
.I value
; the path is canonicalized before testing to eliminate symbolic links, etc.
.RE
.TP 4
.I value
left-hand side of binary operators.  For the file type operators, one of the following strings:
.RS 4
.IP \(bu 4
.I file
.IP \(bu 4
.I directory
.IP \(bu 4
.I link
.IP \(bu 4
.I fifo
.IP \(bu 4
.I socket
.RE
.TP 4
.I message
string that will be displayed if the predicate is not satisfied
.PP
.RE
For example, the following set of dependencies must all evaluate to true AFTER all of the environment actions are performed in order for the
.B vpkg_require gaussian/g09d02
to succeed:
.PP
.RS 4
.nf
"dependencies": [
  { "stage": "post-condition", "variable": "GAUSS_SCRDIR", "operator": "is-set",
    "message": "Gaussian requires GAUSS_SCRDIR be set"
  },
  { "stage": "post-condition", "variable": "GAUSS_SCRDIR", "operator": "!/>",
    "value": "/home",
    "message": "Home directories should not be used as Gaussian scratch directories."
  },
  { "stage": "post-condition", "variable": "GAUSS_SCRDIR", "operator": "-t",
    "value": "directory",
    "message": "A scratch directory must exist for Gaussian jobs."
  }
]
.fi
.RE
.PP
The environment variable indicating where Gaussian should write scratch files must be set.  It must not be a path under the "/home" directory and must exist (Gaussian does not create missing scratch directories itself).  Post-conditions are used here under the assumption that the
.B valet
package definition for
.I gaussian
includes some actions that set GAUSS_SCRDIR to a default value.  If the package were defined under the assumption that the user MUST set GAUSS_SCRDIR, then pre-conditions would be desirable.
.RE
