.TH VALET 5 2016-11-07 "" "VALET Configuration File"


.SH NAME
valet \- system-wide configuration file


.SH DESCRIPTION
When
.B valet
is installed the system administrator may tweak the program's behavior by editing the
.IR config.py
file, found inside the
.IR lib/python/valet
directory.  Changes should be made to the variables defined on the
.B config
Python class therein.
.SH CLASS VARIABLES
Each of the class variables controls some aspect of VALET's behavior:
.PP
.B prefixDir
.RS
The path under which VALET has been installed.  For example, following the preferred software versioning scheme promoted by VALET, we install each version under
.IR /opt/shared/valet/<version>
rather than installing it into
.IR /usr/local
so that multiple versions are easily supported.  The
.B prefixDir
variable defaults to the value of the VALET_PREFIX environment variable or
.I /usr/local
if the variable is not present.
.RE
.B binDir
.RS
The path under which VALET's user executables directory has been installed.  Defaults to the value of
.B prefixDir
with the path component
.I bin
appended (e.g.
.I /usr/local/bin
).
.RE
.B libExecDir
.RS
The path under which helper scripts provided by the system administrator can be found.  For example, the Intel compiler has a complex environment initialization via scripts the the company provides.  Defaults to the value of
.B prefixDir
with the path component
.I libexec
appended (e.g.
.I /usr/local/libexec
).
.RE
.B sysConfDir
.RS
The path(s) under which package definition files provided by the system administrator can be found.  Defaults to a list containing the
.I .valet
directory inside the current user's home directory and the value of
.B prefixDir
with the path component
.I etc
appended (e.g. [
.I ~/.valet
,
.I /usr/local/etc
]).  Note that users can augment this list by means of the VALET_PATH environment variable.
.RE
.B blessedSysConfDir
.RS
The path(s) under which the system administrator explicitly allows "executable" package definition files to be used.  Defaults to the same list contained in the
.B sysConfDir
variable.  Note that users can augment this list by means of the VALET_BLESSED_PATH environment variable.
.RE
.B isVerboseByDefault
.RS
Boolean variable that controls whether or not VALET should produce additional output to stdout.  Defaults to False.  Note that the VALET shell commands all override this default by means of the --verbose flag or the VALET_VERBOSE environment variable.
.RE
.B shouldUseYAMLSafeLoadMethod
.RS
Boolean variable that controls which variant of the PyYAML parser should be used.  If True, then only the safe_load() method is used and PyYAML extensions to the YAML grammar are not allowed.  Defaults to True.
.RE
.B shouldAddFeatureTagsToVersionPrefixes
.RS
Boolean variable that controls whether or not feature tags should be included in the prefix directory auto-generated when a package version has no explicit prefix configured with it.  The tags are sorted in ascending order and joined with a plus sign (+) then appended to the version id atom with a minus sign (-).  For example, openmpi/1.8.2:intel,lustre-io would have the version prefix
.I 1.8.2-intel+lustre-io
auto-generated for it.  Defaults to True.
.RE
.B hwArchTags
.RS
When an executables directory "bin" is configured in a VALET package definition and this flag is True, the
.B hwArchTags
list of sub-paths that are extant under the "bin" directory can be added to the PATH in preference to the "bin" directory itself.  For example, for the
.I hwArchTags
list [x86_64, intel] the relative paths "bin/intel," "bin/x86_64," and "bin" might be added to the PATH in that order from left to right.  The order of the strings in this list is important:  least specific hardware variant to most specific.  The
.B shouldUseHardwareArchTags
variable controls whether or not the
.B hwArchTags
get used.  This variable defaults to an empty list, which signals that the
.I valet_hwarch
helper program should be used to populate the list.  On heterogeneous clusters with jobs partitioned across node classes this would allow the PATH and LD_LIBRARY_PATH to be adjusted automagically by VALET.
.RE
.B shouldUseHardwareArchTags
.RS
Boolean variable that controls whether or not the
.B hwArchTags
sub-paths should be used.
.RE
.B featureMatchingMode
.RS
Selects the default feature-matching mode.  When multiple versions of a package satisfy the features requested by the user, VALET can choose either the
.I first_match
from the list; the version for which the
.I least_additional
features are present; or the version for which the
.I most_additional
features are present.  Note that the user can override this behavior using the VALET_FEATURE_MATCH_MODE environment variable.
.RE
.B shouldAddPackagePrefixAction
.RS
Boolean variable that controls whether or not an action should be automatically added for every package such that an environment variable named <pkg-id>_PREFIX gets set when the "development" context is chosen.  For example, the "mct" package with version prefix
.I /opt/shared/mct/1.8.3
would yield the environment variable MCT_PREFIX set to that path.  This can be very useful when doing e.g. a "./configure --with-mct=$MCT_PREFIX ..." software build configuration that depends on the MCT package.  Defaults to True.
.RE
.B isExecStyleConfigDisabled
.RS
Boolean variable that controls whether or not "executable" package definition formats are allowed.  Even when this variable is False, only those "executable" package definition files present in blessed paths are allowed.  Defaults to False.
.RE
.B isPackageCountingEnabled
.RS
Boolean variable that controls whether or not the "package counting" extension is enabled.  Package counting means that the
.I vpkg_require
and
.I vpkg_devrequire
commands will also do an HTTP-based request that is meant to record package "hits" that the system administrator can use to determine which packages are most popular, etc.  Defaults to False since this feature requires additional configuration of a web server and the
.I valet_count
and
.I valet_query
compiled utilities.
.RE
.PP
Perhaps the most important variables present in the
.B config
class are the sub-paths VALET automatically recognizes for executables, libraries, man pages, etc.
.PP
.B binPathDefaults
.RS
List of sub-paths under which executables are found.  Defaults to [bin, sbin].
.RE
.B libPathDefaults
.RS
List of sub-paths under which libraries (shared and otherwise) are found.  Defaults to [lib, lib64, libso].
.RE
.B manPathDefaults
.RS
List of sub-paths under which man pages are found.  Defaults to [man, share/man].
.RE
.B infoPathDefaults
.RS
List of sub-paths under which Linux info pages are found.  Defaults to [share/info].
.RE
.B includePathDefaults
.RS
List of sub-paths under which header files are found.  Defaults to [include].
.RE
.B pkgConfigPathDefaults
.RS
List of sub-paths under which GNU pkg-config files are found.  Defaults to [lib/pkgconfig, share/pkgconfig].
.RE


.SH ENVIRONMENT VARIABLES
.B VALET_PREFIX
.RS
The path under which VALET is installed.
.RE
.B VALET_VERBOSE
.RS
Overrides the default verbosity configured by the system administrator.  Values indicating boolean false are (case does not matter):  n, no, f, false, 0.  Values indicating boolean true are (case does not matter):  y, yes, t, true, and non-zero integers.
.RE
.B VALET_PATH
.RS
Augments the
.B sysConfDir
list configured by the system administrator.  The value should be one or more filesystem paths, separated by the OS's path separator (e.g. for Linux the colon, :).  Paths provided in this variable precede the list configured by the system administrator and are searched in the order they are provided.
.RE
.B VALET_BLESSED_PATH
.RS
Augments the
.B blessedSysConfDir
list configured by the system administrator.
.RE
.B VALET_FEATURE_MATCH_MODE
.RS
Overrides the default mode configured by the system administrator.  Acceptable values are
.I first_match
,
.I least_additional
, or
.I most_additional
.RE
