.TH VALET 5 2016-11-03 "" "VALET Package Definition Files"
.SH NAME
valet \- XML package definition file format
.SH DESCRIPTION
.B valet
originally used XML as its sole file format.  XML package definition files use the
.B .vpkg
or
.B .vpkg_xml
extension.  The original (version 1) syntax remains viable through version 3 of
.B valet
and both versions 2 and 3 have augmented the schema slightly.
.SS PACKAGE
A
.B valet
package is represented in XML as a document with the following overall structure:
.PP
.RS 4
.nf
<?xml version="1.0" encoding="UTF-8"?>
<package id="packageid">
    :
  <version id="versionid">
    :
  </version>
</package>
.fi
.RE
.PP
The
.I <package>
document element (and all
.I <version>
elements) must have an "id" attribute and may have a "context" attribute to limit the availability of the package (or version).  The "context" attribute should be a comma-separated list of context names.
.PP
A set of scalar properties can be used both in the
.I package
and
.I version
elements of the configuration. Scalar properties are:
.PP
.TP 4
<description>text...</description>
Human-readable description of the software package
.TP 4
<url>hyperlink</url>
A web site documenting the package, for example
.TP 4
<prefix>path</prefix>
A filesystem path containing the package or the version of the package
.TP 4
<development-env/>
.B valet
should emit LDFLAGS and CPPFLAGS
.TP 4
<no-development-env/>
.B valet
should not emit LDFLAGS and CPPFLAGS
.TP 4
<standard-paths/>
.B valet
should check for standard sub-paths like "bin" and "lib" under the prefix directory
.TP 4
<no-standard-paths/>
.B valet
should not check for standard sub-paths like "bin" and "lib" under the prefix directory
.TP 4
<context>context-name</context>
As an alternative to using the "contexts" attribute on a
.I <package>
or
.I <version>
element, each context can be specified using one of these elements.  Zero or more
.I <context>
elements are permissible.
.TP 4
<flag>flag-name</flag>
Add the given
.I flag-name
to the set of flags for the package or versioned package.  Currently defined flags include
.PP
.RS 4
.IP \(bu 4
no-standard-paths
.IP \(bu 4
no-development-env
.PP
Zero or more
.I <flag>
elements are permissible.
.RE
.PP
The default version of the package (to be used when the user provides no version id to
.B vpkg_require
and related commands) can be indicated using the
.I <default-version>version-id</default-version>
element.
.PP
Typically, the package-global
.I <prefix>
is set to a directory containing each version of the package, and each versioned package defines a prefix relative to that directory:
.PP
.RS 4
.nf
$ ls -l /opt/shared/openmpi
total 14
drwxr-sr-x 8 user   sysadmin 8 Aug 27 16:10 1.8.2
drwxr-sr-x 8 user   sysadmin 8 Aug 27 16:10 1.8.2-gcc-4.8.3
drwxr-sr-x 8 user   sysadmin 8 Aug 28 09:26 1.8.2-intel64
drwxrwsr-x 2 user   sysadmin 3 Aug 27 17:10 attic
.fi
.RE
.PP
The package's prefix would be "/opt/shared/openmpi" (absolute path required) and versions would use e.g. a prefix of "1.8.2-intel64" (relative path, absolute is permissible, too).  If no prefix is provided for a versioned package, then
.B valet
implicitly uses the version id.
.SS VERSION
Versions of a package are represented as a
.I <version>
element:
.PP
.RS 4
.nf
<package id="gaussian">
  <description>Gaussian Quantum Chemistry Suite</description>
  <url>http://gaussian.com/</url>
  <prefix>/opt/shared/gaussian</prefix>
  <flag>no-standard-paths</flag>
  <flag>no-development-env</flag>
  <version id="g09">
    <prefix>g09d01</prefix>
  </version>
  <version id="g09d01">
  </version>
</package>
.fi
.RE
.PP
In this example, version "g09d01" has a prefix path of "/opt/shared/gaussian/g09d01" since it did not specify a prefix. Version "g09" provided a relative path that is appended to the prefix of its parent package to also yield "/opt/shared/gaussian/g09d01." Both versions inherit the "no-development-env" and "no-standard-paths" flags of the parent package.
.PP
The full versioned package identifiers available in the gaussian package defined above would be:
.IP \(bu 4
gaussian/g09
.IP \(bu 4
gaussian/g09d01
.PP
Since the "g09" version targets the same executables as "g09d01," the "g09" version can be considered to be an alias of "g09d01."
.B valet
allows for aliased versions of packages:
.PP
.RS 4
.nf
<version id="g09" alias-to="g09d01"/>
.fi
.RE
.PP
Versioned packages that do not specify the
.I <development-env>
,
.I <no-development-env>
,
.I <standard-paths>
, or
.I <no-standard-paths>
elements inherit the value of their parent package.
.TP 4
<feature>tag</feature>
Later releases in the
.B valet
1 series added this element as a simple notational convenience; when feature tags were added in
.B valet
2, the tag text in these elements began to be added to the feature tags on the versioned package id.  Since feature tags use a specific syntax, the arbitrary text that a version 1
.I <feature>
element allows must be transformed:
.RS 4
.IP 1. 4
ranges of whitespace become a single underscore
.IP 2. 4
ranges of characters not allowed in feature tags are removed
.IP 3. 4
the longest match (anchored at the end of the string) with the feature tag syntax is extracted
.PP
The resulting string is added to the version's feature tag set.
.RE
.PP
Once the overall versioning structure of a package has been created, the environment tests and changes associated with the package and versions of the package can be added. The three kinds of tests/changes are:
.TP 4
.I <dependencies>
other packages that must be present or loaded into the environment; environment variable tests that must evaluate to true for configuration to happen
.TP 4
.I <incompatibilities>
other packages that must NOT be present in the environment; environment variable tests that must evaluate to false for configuration to happen
.TP 4
.I <actions>
changes to be made to the environment if all dependencies and incompatibilities are satisfied
.PP
If present in the
.I <package>
element, the tests/actions affect all versions of the package and are satisfied/applied before those present inside a
.I <version>
element.  For example:
.PP
.RS 4
.nf
<package
  xmlns="http://www.udel.edu/xml/valet/1.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.udel.edu/xml/valet/1.0
                      http://www.udel.edu/xml/valet/1.0/schema.xsd"
  id="gaussian">
  <description>Gaussian Quantum Chemistry Suite</description>
  <url>http://gaussian.com/</url>
  <prefix>/opt/shared/gaussian</prefix>
  <flag>no-standard-paths</flag>
  <flag>no-development-env</flag>
  <dependencies>
    <predicate stage="pre-condition">
      <variable>GAUSS_SCRDIR</variable>
      <operator>not-is-set</operator>
      <message>If GAUSS_SCRDIR is not set, the working directory will be used; you do not want that.</message>
    </predicate>
  </dependencies>
  <actions>
    <executable shell="any" method="exec" order="success-first" succeed="0">mk-gaussian-scrdir</executable>
  </actions>
  <version id="g09" alias-to="g09d01"/>
  <version id="g09d01">
    <dependencies>
      <package id="pgi/14"/>
    </dependencies>
    <actions>
      <export variable="GAUSSIAN_VERSION" operator="set">G09</export>
      <executable method="source" shell="sh">g09.sh</executable>
    </actions>
  </version>
</package>
.fi
.RE
.PP
Given the package definition above, typing
.B vpkg_require gaussian/g09
yields the following course of events:
.IP 1. 4
g09 is an alias to g09d01, so version g09d01 becomes the target versioned package being configured
.IP 2. 4
The prefix for the version is constructed as /opt/shared/gaussian/g09d01
.IP 3. 4
The GAUSS_SCRDIR environment variable is examined
.RS 4
.IP \(bu 4
If it does not exist in the environment the configuration fails and the message text is printed to stdout
.RE
.IP 4. 4
The "mk-gaussian-scrdir" program found in
.B valet
\[char39]s libexec directory is executed
.RS 4
.IP \(bu 4
If the result code returned by "mk-gaussian-scrdir" is zero, continue
.IP \(bu 4
Otherwise, the configuration fails and exits
.RE
.IP 5. 4
Has the pgi/14 package been loaded into the environment yet?
.RS 4
.IP \(bu 4
If not, do all of its tests and attempt to affect all of its environment changes before proceeding
.IP \(bu 4
If that fails, this configuration fails and must exit immediately
.RE
.IP 6. 4
The GAUSSIAN_VERSION environment variable is assigned the value G09
.IP 7. 4
For Bash and other sh-like shells, the script g09.sh in
.B valet
\[char39]s libexec directory is sourced
.RS 4
.IP \(bu 4
The return code is not checked for success or failure
.RE
.PP
If control reaches the end of this pseudo-code, then the gaussian/g09 package has been successfully configured in the environment.
.SS ACTIONS
An XML
.I <actions>
element can be specified either in a
.I <package>
or in a
.I <version>
of a package and contains zero or more elements of the following kinds:
.TP 4
variable
make changes to environment variables
.TP 4
special directories
shorthand for alterations to PATH, LD_LIBRARY_PATH, etc.
.TP 4
executable
source a script or execute a program
.TP 4
shell alias
set/unset an alias
.PP
Each action is represented as an XML element. Actions attached to the package itself will be performed when any version of the package is selected for addition to the environment, and in addition to actions associated with the version itself.
.PP
Actions are performed in the order they are specified.  Example:
.PP
.RS 4
.nf
<actions>
  <incdir>/opt/include</incdir>
  <export variable="PATH" operator="scrub-path">/opt/bin</export>
  <bindir>/opt/bin</bindir>
  <libdir>/opt/lib</libdir>
  <libdir>/opt/lib64</libdir>
  <mandir>/opt/share/man</mandir>
  <export variable="USER_SW_PATH">/opt/bin</export>
  <export variable="CFLAGS" operator="append-space" contexts="development">-O3 -g</export>
</actions>
.fi
.RE
.PP
.RS 4
.SS Variable actions
Variable actions alter the value of an environment variable.  A variable action is an XML
.I <export>
element with the following attributes:
.TP 4
variable
Environment variable name	(required)
.TP 4
operator
One of the following:
.RS 4
.IP \(bu 4
set (default)
.IP \(bu 4
unset
.IP \(bu 4
append
.IP \(bu 4
prepend
.IP \(bu 4
append-path
.IP \(bu 4
prepend-path
.IP \(bu 4
append-space
.IP \(bu 4
prepend-space
.IP \(bu 4
scrub
.IP \(bu 4
scrub-path
.RE
.TP 4
contexts
A list of the contexts under which this action should be performed (defaults to "all")
.PP
The text inside the
.I <export>
element is the value to add/remove/etc.
.PP
The operators are self-explanatory for the most part.  The
.I scrub
operator removes all occurrences of value from the variable's current value in the environment.  The
.I scrub-path
operator treats the value of variable as a search path and removes from that search path all occurrences (by exact match) of value.
.PP
The value may contain references to other environment variables using the syntax ${ENV VAR NAME}.
.PP
.RS 4
.nf
<export variable="PATH" operator="scrub-path" contexts="development">${HOME}/bin</export>
.fi
.RE
.SS Special directories
All special-directory actions are enclosed in an XML element within the
.I <actions>
element. These actions are shortcuts that produce variable actions that affect standard environment variables (like PATH or LD_LIBRARY_PATH). The following XML elements are available:
.TP 4
<bindir>path</bindir>
Add
.I path
to PATH
.TP 4
<libdir>path</libdir>
Add
.I path
to LD_LIBRARY_PATH (and LDFLAGS under the "development" context)
.TP 4
<incdir>path</incdir>
Add
.I path
to CPPFLAGS under the "development" context
.TP 4
<mandir>path</mandir>
Add
.I path
to MANPATH
.TP 4
<infodir>path</infodir>
Add
.I path
to INFOPATH
.TP 4
<pkgconfigdir>path</pkgconfigdir>
Add
.I path
to PKG_CONFIG_PATH
.PP
Each element can be used zero or more times in any
.I <actions>
block.
.PP
.RS 4
.nf
<actions>
    :
  <bindir>/opt/bin</bindir>
  <libdir>/opt/lib</libdir>
  <libdir>/opt/lib64</libdir>
  <mandir>/opt/share/man</mandir>
    :
</actions>
.fi
.RE
.PP
.SS Executables
At times
.B valet
may not be able to fully express the modifications necessary for a package, or the package includes its own extensive environment configuration scripts.  In such instances, a
.B valet
package can be configured to source an external script or execute a program.  An
.I <executable>
action has the following attributes:
.TP 4
method
Either "source" (default) or "exec"
.TP 4
order
Either "failure-first" or "success-first"
.TP 4
success
integer return code indicating successful execution/sourcing
.TP 4
failure
integer return code indicating failed execution/sourcing
.TP 4
shell
shell type for which this script is executed (e.g. "sh" or "bash")
.TP 4
contexts
A list of the contexts under which this action should be performed (defaults to "all")
.PP
The text inside the XML
.I <executable>
element is the path to the script or program to be sourced/executed.  Multiple
.I <executable>
elements are allowed.
.PP
The order determines which is tested first, the success return code or the failure return code.  If no success or failure are specified, then the return code is not tested.  Many shell scripts return code 0 on success and a non-zero value in case of a failure; this could be tested by supplying order "success_first" and success "0."
.PP
.RS 4
.nf
<executable method="source" order="success-first" success="0" shell="sh">/opt/mathematica/bin/remote_kernel.sh</executable>
<executable method="source" order="success-first" success="0" shell="csh">/opt/mathematica/bin/remote_kernel.csh</executable>
.fi
.RE
.SS Shell aliases
An alias is a word that represents an arbitrarily complex command in the shell environment.  Rather than typing the command "ls -l | less" repeatedly, a user might create an alias named "lll" that when typed yields the same results as if "ls -l | less" had been typed.  A
.I <shell-alias>
action has the following attributes:
.TP 4
name
the word that should be aliased to the arbitrarily complex command
.TP 4
shell
shell type for which this alias should be set (e.g. "sh" or "bash")
.TP 4
contexts
A list of the contexts under which this action should be performed (defaults to "all")
.PP
The text inside the XML
.I <shell-alias>
element is the command to be executed for the alias.  A blank command implies that the named alias should be removed from the environment if it exists.  Multiple
.I <shell-alias>
elements are allowed.
.RE
.SS DEPENDENCIES & INCOMPATIBILITIES
Dependencies and incompatibilities are XML elements that contain one or more package identifiers, package identifier patterns, or predicates that must:
.IP \(bu 4
dependencies:
.RS 4
.IP \(bu 4
package id/pattern must be satisfied (present in environment or successfully loads and configures)
.IP \(bu 4
predicate evaluates to true
.RE
.IP \(bu 4
incompatibilities:
.RS 4
.IP \(bu 4
package id/pattern must not be configured in the current environment
.IP \(bu 4
predicate evaluates to false
.RE
.PP
A predicate can have a
.I stage
attribute that indicates when it should be tested.  A
.I pre-condition
is tested before any environment changes are made for the sake of the package, while a
.I post-condition
is tested after all environment changes have been made for the sake of the package.
.PP
.RS 4
.nf
<dependencies>
  <package id="pgi/14"/>
  <package id="fftw/^^3\."/>
</dependencies>
<incompatibilities>
  <predicate stage="post-condition">
    <variable>GAUSS_SCRDIR</variable>
    <operator>starts-with</operator>
    <value>/home</value>
    <message>Home directories should not be used as scratch for Gaussian.</message>
  </predicate>
</incompatibilities>
.fi
.RE
.RS 4
.SS Package id patterns
A package id pattern is a string that is similar in construction to a regular package id but contains
.IP \(bu 4
a regular expression for the package id, version id, or both
.IP \(bu 4
a version predicate for the version id
.PP
A regular expression is indicated in either half by prefixing with a carat (^) character. For example, the pattern
.PP
.RS 4
openmpi/^^1\\.[0-9]*[02468]\\.
.RE
.PP
includes a regular expression in the version half of the id.  Discarding the leading carat, the regular expression is
.PP
.RS 4
^1\\.[0-9]*[02468]\\.
.RE
.PP
This pattern will match with any even-numbered release of the openmpi package in the 1.x series: 1.4.4, 1.6.3, 1.8.2 would all be matches, but 1.5.1 would not.
.PP
As a dependency, id patterns are tested against packages that have already been loaded into the environment.  If no matching package has been loaded,
.B valet
locates an available package that matches the pattern and attempts to load it into the environment.  Note that
.B valet
chooses the first versioned package it finds as a match, which may or may not be a new enough version, etc.  Ideally, the user will have loaded an appropriate version of the package prior to loading the package that uses an id pattern dependency.
.PP
.RS 4
.B [NOTE]:
Using a regular expression or version predicate that is the logical negation of that which you would use for a dependency (thus employing an incompatibility) forces the user to manually load the dependency prior to loading this package.  This is because package ids and id patterns inside an
.I <incompatibilities>
element do not cause
.B valet
to attempt to load a package into the environment.
.PP
A version predicate is prefixed with an equals (=) character and outlines numerical version tests to be made against the defined versions of a package.  For example, selecting any release of Open MPI after and including 1.4.4 but prior to 1.10 uses the pattern:
.PP
.RS 4
openmpi/=>=1.4.4&<1.10
.RE
.PP
The syntax is simple:  discarding the leading equals (=) character, one or more tests are linked using logical AND (&) and logical OR (|).  Each test consists of a binary operator and a version number:
.PP
.RS 4
  (=|==|<|<=|>=|>)(X(.Y(.Z)?)?)
.RE
.PP
The left-hand side of the test is the primary version number (the first found moving from left to right) that
.B valet
finds in the version id.  So the version id "1.10.2+intel2016+lustre" would have a primary version number "1.10.2" while the version id "release-candidate-2016" would have a primary version number "2016."  If a version id has no numeric versioning components present, then no version predicate will match it (for example, version id "os-native").  When specifying the right-hand side, only the first numerical component is required.  Omitting the dotted second or third components, the predicate will only be against the provided components.  Thus, a L.H.S. version id "1.10.2" matches all of the following predicates:
.PP
.IP \(bu 4
=1.10.2
.IP \(bu 4
=1.10
.IP \(bu 4
=1
.IP \(bu 4
>=1.10
.IP \(bu 4
<2
.PP
For "=1.10" and "=1" the missing second and third version numbers are NOT assumed to be zero, but are simply not included in the comparison.  Since "1.10.2" has it's first numerical component "1" equal to "1," the predicate "=1" is satisfied.  Likewise, since the second numerical component "10" equals "10," the predicate "=1.10" is also satisfied.
.SS Predicates
A predicate is a test performed against an environment variable's value to determine whether package configuration should succeed or fail.  Minimally, an XML
.I <predicate>
element must contain an environment variable name and an operator that specifies what test is to be performed.
.PP
A
.I <predicate>
element should have a
.I stage
attribute that has one of the following values:
.IP \(bu 4
.I pre-condition
(default):  test must be satisfied BEFORE configuration changes are made
.IP \(bu 4
.I post-condition
:  test must be satisfied AFTER configuration changes are made
.PP
There are four child elements of the
.I <predicate>
element:
.TP 4
.I <variable>
environment variable name; for filesystem predicates, the path to be tested
.TP 4
.I <operator>
One of the following:
.RS 4
.IP \(bu 4
.I is-set
: has a value
is-not-set: does not have a value
.IP \(bu 4
.I eq ==
: lexigraphically equivalent to
.I <value>
.IP \(bu 4
.I ne !=
: not lexigraphically equivalent to
.I <value>
.IP \(bu 4
.I lt <
: lexigraphically less-than
.I <value>
.IP \(bu 4
.I le <=
: lexigraphically less-than or equivalent to
.I <value>
.IP \(bu 4
.I gt >
: lexigraphically greater-than
.I <value>
.IP \(bu 4
.I ge >=
: lexigraphically greater-than or equivalent to
.I <value>
.IP \(bu 4
.I starts-with <<
:
.I <value>
as prefix
.IP \(bu 4
.I not-starts-with !<<
: not
.I <value>
as prefix
.IP \(bu 4
.I ends-with >>
:
.I <value>
as suffix
.IP \(bu 4
.I not-ends-with !>>
: not
.I <value>
as suffix
.IP \(bu 4
.I contains <>
: contains
.I <value>
.IP \(bu 4
.I not-contains !<>
: does not contain
.I <value>
.IP \(bu 4
.I matches ~
: matches regular expression in
.I <value>
.IP \(bu 4
.I not-matches !~
: does not match regular expression in
.I <value>
.IP \(bu 4
.I exists -e
: path specified exists on system
.IP \(bu 4
.I not-exists !-e
: path specified does not exist on system
.IP \(bu 4
.I is-file-type -t
: path specified is of
.I <value>
file type
.IP \(bu 4
.I not-is-file-type !-t
: path specified is not of
.I <value>
file type
.IP \(bu 4
.I is-strict-file-type -st
: path specified is of
.I <value>
file type (symlinks not followed)
.IP \(bu 4
.I not-is-strict-file-type !-st
: path specified is not of
.I <value>
file type (symlinks not followed)
.IP \(bu 4
.I is-readable -r
: path specified is readable by current user
.IP \(bu 4
.I not-is-readable !-r
: path specified is not readable by current user
.IP \(bu 4
.I is-writable -w
: path specified is writable by current user
.IP \(bu 4
.I not-is-writable !-w
: path specified is not writable by current user
.IP \(bu 4
.I is-executable -x
: path specified is executable by current user
.IP \(bu 4
.I not-is-executable !-x
: path specified is not executable by current user
.IP \(bu 4
.I path-under is-descendent-of />
: path specified is a descendent path of
.I <value>
; the path is canonicalized before testing to eliminate symbolic links, etc.
.IP \(bu 4
.I not-path-under not-is-descendent-of !/>
: path specified is not a descendent path of
.I <value>
; the path is canonicalized before testing to eliminate symbolic links, etc.
.RE
.TP 4
.I <value>
left-hand side of binary operators.  For the file type operators, one of the following strings:
.RS 4
.IP \(bu 4
.I file
.IP \(bu 4
.I directory
.IP \(bu 4
.I link
.IP \(bu 4
.I fifo
.IP \(bu 4
.I socket
.RE
.TP 4
.I <message>
string that will be displayed if the predicate is not satisfied
.PP
.RE
For example, the following set of dependencies must all evaluate to true AFTER all of the environment actions are performed in order for the
.B vpkg_require gaussian/g09d02
to succeed:
.PP
.RS 4
.nf
<dependencies>
  <predicate stage="post-condition">
    <variable>GAUSS_SCRDIR</variable>
    <operator>is-set</operator>
    <message>Gaussian requires GAUSS_SCRDIR be set.</message>
  </predicate>
  <predicate stage="post-condition">
    <variable>GAUSS_SCRDIR</variable>
    <operator>!/&lt;</operator>
    <value>/home</value>
    <message>Home directories should not be used as Gaussian scratch directories.</message>
  </predicate>
  <predicate stage="post-condition">
    <variable>GAUSS_SCRDIR</variable>
    <operator>-t</operator>
    <value>directory</value>
    <message>A scratch directory must exist for Gaussian jobs.</message>
  </predicate>
</dependencies>
.fi
.RE
.PP
The environment variable indicating where Gaussian should write scratch files must be set.  It must be a path under the "/home" directory and must exist (Gaussian does not create missing scratch directories itself).  Post-conditions are used here under the assumption that the
.B valet
package definition for
.I gaussian
includes some actions that set GAUSS_SCRDIR to a default value.  If the package were defined under the assumption that the user MUST set GAUSS_SCRDIR, then pre-conditions would be desirable.
.RE
