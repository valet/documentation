# Official VALET Documentation

This project encompasses all external documentation associated with the VALET project.  It is organized by format:

* ./html : Simple HTML layout
* ./man  : Man pages
